#!/usr/bin/env python3

import argparse
from hashlib import md5

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--login", help="Digest login")
parser.add_argument("-p", "--password", help="Digest password")
parser.add_argument("-u", "--uri", help="SIP URI (including 'sip:' prefix, e.g. sip:127.0.0.1)")
parser.add_argument("-n", "--nonce", help="Server-side nonce")
parser.add_argument("-r", "--realm", help="Realm")
args = parser.parse_args()

args_dict = vars(args)

print("\nProvided arguments: " + str(args_dict))

for arg,arg_value in vars(args).items():
    if arg_value is None:
        print("Arguments error. Please check arguments and refer to help (-h, --help)\n")
        exit(1)


digest1 = md5(("{}:{}:{}".format(args_dict['login'],args_dict['realm'],args_dict['password'])).encode('utf-8')).hexdigest()
digest2 = md5(("REGISTER:{}".format(args_dict['uri'])).encode('utf-8')).hexdigest()
response_digest = md5(("{}:{}:{}".format(digest1,args_dict['nonce'],digest2)).encode('utf-8')).hexdigest()

print("Response digest: " + response_digest + "\n")

